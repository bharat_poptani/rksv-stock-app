angular.module('stockApp')
	.factory('socket', ['$rootScope', function ($rootScope) {
  	var socket = io.connect('http://kaboom.rksv.net/watch');
  	return {
  		on: function(eventName, callback) {
  			socket.on(eventName, function() {
  				var args = arguments;
  				$rootScope.$apply(function() {
  					callback.apply(socket, args);
  				});
  			});
  		},
  		emit: function(eventName, data, callback) {
  			socket.emit(eventName, data, function() {
  				var args = arguments;
  				$rootScope.$apply(function () {
  					if (callback) {
  						callback.apply(socket, args);
  					}
  				});
  			});
  		}
  	};
  }])
  .factory('historicalStocks', ['$http', function ($http) {
  	return {
  		'getData': function() {
  			return $http.get('http://kaboom.rksv.net/api/historical');
  		}
  	};
  }])
  .factory('formatCSV', function() {
  	return {
  		'formatResponse': function(response) {
  			var stock;
  			if (typeof response === 'string') {
  				var OHLC = response.split(',');
  				OHLC.splice(OHLC.length - 1, 1);
  				stock = this.formatStock(OHLC);
  				return stock;
  			} else if (Array.isArray(response)) {
  				var stockValues = [];

  				for(var i=0; i<response.length; i++) {
  					var OHLC = response[i].split(',');
  					OHLC.splice(OHLC.length - 1, 1);
  					stockDetails = this.formatStock(OHLC);
  					stockValues.push(stockDetails);
  				}

  				return stockValues;
  			}
  		},
  		'formatStock' : function(formattedCSV) {
  			var stockObject = {};
  			//timestamp, open, high, low, close, volume
  			stockObject['date'] = parseFloat(formattedCSV[0]);
  			stockObject['open'] = parseFloat(formattedCSV[1]);
  			stockObject['high'] = parseFloat(formattedCSV[2]);
  			stockObject['low'] = parseFloat(formattedCSV[3]);
  			stockObject['close'] = parseFloat(formattedCSV[4]);
  			stockObject['volume'] = parseFloat(formattedCSV[5]);
  			return stockObject;
  		}
  	};
  })
  .factory('calcStats', function() {
    return {
      'fiftyTwoWeekStats': function(formatStats) {
        var fiftyTwoWeekStats = formatStats.slice(formatStats.length - 365, formatStats.length);

        var maxHigh = function(array) {
          return Math.max.apply(Math, array.map(function(stats){
            return stats.high;
          }));
        };

        var maxLow = function(array) {
          return Math.min.apply(Math, array.map(function(stats){
              return stats.high;
          }));
        };

        var fiftyTwoWeekHigh = maxHigh(fiftyTwoWeekStats);
        var fiftyTwoWeekLow = maxLow(fiftyTwoWeekStats);

        var stats = {
          'high': fiftyTwoWeekHigh,
          'low': fiftyTwoWeekLow
        };

        return stats;
      }
    };
  })
  .factory('plotChart', function() {
  	return {
  		'options': {
  			chart: {
  				type: 'ohlcBarChart',
  				height: 450,
  				margin : {
  					top: 50,
  					right: 50,
  					bottom: 50,
  					left: 100
  				},
  				x: function(d){ return d['date']; },
  				y: function(d){ return d['close']; },
  				duration: 100,

  				xAxis: {
  					axisLabel: 'Dates',
  					tickFormat: function(d) {
  						return d3.time.format('%x')(new Date(d));
  					},
  					showMaxMin: false
  				},

  				yAxis: {
  					axisLabel: 'Stock Price',
  					tickFormat: function(d){
  						return d3.format(',.1f')(d);
  					},
  					showMaxMin: false
  				}
  			}
  		}
  	};
  });
