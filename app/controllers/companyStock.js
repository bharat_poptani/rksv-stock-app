angular.module('stockApp')
	.controller('companyStockCtrl', ['socket', 'historicalStocks', 'formatCSV', 'calcStats', 'plotChart', function(socket, historicalStocks, formatCSV, calcStats, plotChart){
		var company = this;
		company.name = 'T\'zikal Beauty';

		//Company Stock Live value
		socket.on('connect', function() {
			var CLIENT_ACKNOWLEDGEMENT = 1;
			socket.emit('sub', {'state': true}, function(data) {
				console.log(data);
			});
			socket.on('data', function(data) {
				//Formatting the response
				company.liveStock = formatCSV.formatResponse(data);
			});
			socket.on('error', function(error) {
				if (error) {
					company.stock.error = 'There is a problem with your connection, please refresh your browser or try again later';
				}
			});
		});

		//Historial Stock Values
		company.getHistoricalData = function() {

			company.options = plotChart.options;

			historicalStocks
				.getData()
				.then(function(res) {
					var values = formatCSV.formatResponse(res.data);

					company.allStockValues = values;

					company.stock = {
						'open': values[values.length-1].open,
						'close': values[values.length-1].close,
						'high': values[values.length-1].high,
						'low': values[values.length-1].low,
						'volume': values[values.length-1].volume
					};

					company.fiftyTwoWeekStats = calcStats.fiftyTwoWeekStats(values);

					company.data = [
						{
							'values': values
						}
					];
				});

			};

			//Timeline
			company.timelines = ['1W', '1M', '3M', '6M', '1Y', 'ALL'];

			//Default Tab
			company.currentTab = 'ALL';

			company.onClickTab = function (timeline) {

					var stockDetailsLength = company.allStockValues.length;

					//Switches the currentTab to the timeline's current tab
	        company.currentTab = timeline;

	        switch(timeline) {
	        	case '1W':
	        		var oneWeekDetails = company.allStockValues.slice(stockDetailsLength-7, stockDetailsLength);

	        		company.data = [{
	        			'values': oneWeekDetails
	        		}];
	        		break;

	        	case '1M':
	        		var oneMonthDetails = company.allStockValues.slice(stockDetailsLength-31, stockDetailsLength);

	        		company.data = [{
	        			'values': oneMonthDetails
	        		}];
	        		break;

	        	case '3M':
	        		var threeMonthsDetails = company.allStockValues.slice(stockDetailsLength-90, stockDetailsLength);

	        		company.data = [{
	        			'values': threeMonthsDetails
	        		}];
	        		break;
	        	case '6M':
	        		var sixMonthsDetails = company.allStockValues.slice(stockDetailsLength-180, stockDetailsLength);

	        		company.data = [{
	        			'values': sixMonthsDetails
	        		}];
	        		break;

	        	case '1Y':
	        		var oneYearDetails = company.allStockValues.slice(stockDetailsLength-365, stockDetailsLength);

	        		company.data = [{
	        			'values': oneYearDetails
	        		}];
	        		break;

	        	case 'ALL':
	        		company.data = [{
	        			'values': company.allStockValues
	        		}];
	        		break;

	        	default:
	        		company.data = [{
	        			'values': company.allStockValues
	        		}];
	        		break;
	        }
	    };

	    company.isActiveTab = function (timeline) {
	        return timeline === company.currentTab;
	    };
		}

	]);
